package com.example.davalebashvidi.Extensions

import android.view.View

fun View.setVisible() :Unit
{
    this.visibility=View.VISIBLE
}
