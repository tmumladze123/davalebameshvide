package com.example.davalebashvidi

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.InputType.*
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.davalebashvidi.Extensions.isValidEmail
import com.example.davalebashvidi.Extensions.setVisible
import com.example.davalebashvidi.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.password.setEndIconOnClickListener {
            if(binding.passwordText.transformationMethod==PasswordTransformationMethod.getInstance())
            {
                binding.passwordText.transformationMethod= HideReturnsTransformationMethod.getInstance()
                binding.password.endIconDrawable=getDrawable(R.drawable.ic_component_1___12)
                binding.passwordText.setSelection(binding.passwordText.text.toString().length)
            }
            else
            {
                binding.passwordText.transformationMethod= PasswordTransformationMethod.getInstance()
                binding.password.endIconDrawable=getDrawable(R.drawable.ic_component_1___11)
                binding.passwordText.setSelection(binding.passwordText.text.toString().length)
            }
        }
    }

    fun clickSignIn(view:View)
    {
        if(!binding.emailText.text.toString().isValidEmail() || binding.passwordText.text.toString().isEmpty() )
        {
            binding.error.setVisible()
            val drawable = (binding.emailText.background as GradientDrawable).apply {
                setStroke(3, Color.RED)
            }
        }

    }





}